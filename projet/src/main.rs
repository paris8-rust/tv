use fltk::{app::*, button::*, window::*};
use webbrowser;


// Tab des liens 

fn lien(a:usize){

    let vec = vec!["https://www.programme-tv.net/programme/chaine/programme-tf1-19.html",
    "https://www.programme-tv.net/programme/chaine/programme-france-2-6.html",
    "https://www.programme-tv.net/programme/chaine/programme-france-3-7.html",
    "https://www.programme-tv.net/programme/chaine/programme-canalplus-2.html",
    "https://www.programme-tv.net/programme/chaine/programme-france-5-9.html",
    "https://www.programme-tv.net/programme/chaine/programme-m6-12.html",
    "https://www.programme-tv.net/programme/chaine/programme-arte-337.html",
    "https://www.programme-tv.net/programme/chaine/programme-c8-4.html",
    "https://www.programme-tv.net/programme/chaine/programme-w9-24.html",
    "https://www.programme-tv.net/programme/chaine/programme-tmc-21.html"];

    webbrowser::open(vec[a]).expect("failed to open URL");
}



#[derive(Debug)]

struct Corp;

struct Vue {
    
    corp :Corp,
}

struct Chargement {

    vue : Vue,
}


// création de l'interface 

impl Vue{

    fn fenetre(&self){

        let app = App::default();

        self.corp.construction();
        app.run().unwrap(); 
        
         

    } 


}

// création de l'ensemble des boutons .

impl Corp {

    fn construction(&self){

        let mut wind = Window::new(580, 90, 400, 650, "Télécommande");
        wind.set_color(Color::from_u32(0xffebee));

        let chaine = vec!["Tf1","France 2","France 3","Canal +","France 5"," M6 "," Arte "," D8 "," W9 "," TMC "];
        let mut numero:usize = 0;
        let mut hauteur:i32 = 30;
      
        let iter = chaine.iter();
        for x in iter {
            let mut x = Button::new(150,hauteur,80,50, chaine[numero]);
            x.set_callback(move || lien(numero));
            x.set_color(Color::Light3);
            hauteur  += 60;
            numero += 1;
        }
        
        wind.end();
        wind.show();

        
    }

}


// appel de la struct interface 

impl Chargement {

    fn lancement(&self){
        self.vue.fenetre();
        }

}




fn main(){

    let corp = Corp;

    let vue = Vue {

        corp : corp,
    };

    let chargement = Chargement {

        vue : vue,
    };
   
    chargement.lancement();
}
